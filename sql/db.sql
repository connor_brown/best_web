use containerdb;

CREATE TABLE IF NOT EXISTS tbl_products (

    ID INT(11) AUTO_INCREMENT,
    TITLE VARCHAR(20) NOT NULL,
    DESCRIPT VARCHAR(255) NOT NULL,
    PRICE INT(20) NOT NULL,   
    IMG VARCHAR(120) NOT NULL,
    PRIMARY KEY (ID)
) AUTO_INCREMENT = 1;

INSERT INTO tbl_products (TITLE, DESCRIPT, PRICE, IMG) VALUES ('Bone dice', 'dice hand crafted from bone', 40, 'sample.jpg');
INSERT INTO tbl_products (TITLE, DESCRIPT, PRICE, IMG) VALUES ('Warforge starter kit', 'a set of books and dice for playing warforged', 80, 'sample.jpg>');
INSERT INTO tbl_products (TITLE, DESCRIPT, PRICE, IMG) VALUES ('Shadowlord figureine', 'a unpainted miniutine of the shadowlord', 20, 'sample.jpg>');

use containerdb;

SELECT * FROM tbl_products;

CREATE TABLE IF NOT EXISTS tbl_cust (

    ID INT(11) AUTO_INCREMENT,
    FNAME VARCHAR(20) NOT NULL,
    LNAME VARCHAR(20) NOT NULL,
    INVOICE INT(20) NOT NULL,   
    PRIMARY KEY (ID)
) AUTO_INCREMENT = 1;

INSERT INTO tbl_cust (FNAME, LNAME, INVOICE) VALUES ("Lisa", "Dougles", 0);
INSERT INTO tbl_cust (FNAME, LNAME, INVOICE) VALUES ("Tony", "Black", 0);
INSERT INTO tbl_cust (FNAME, LNAME, INVOICE) VALUES ("Argent", "Abbot", 0);
INSERT INTO tbl_cust (FNAME, LNAME, INVOICE) VALUES ("Lucifer", "Morningstar", 0);

use containerdb;

SELECT * FROM tbl_cust;

CREATE TABLE IF NOT EXISTS tbl_staff (

    ID INT(11) AUTO_INCREMENT,
    FNAME VARCHAR(20) NOT NULL,
    LNAME VARCHAR(20) NOT NULL,
    PNUM INT(20) NOT NULL,
    PRIMARY KEY (ID)
) AUTO_INCREMENT = 1;

INSERT INTO tbl_staff (FNAME, LNAME, PNUM) VALUES ("Marten", "White", 02137856);
INSERT INTO tbl_staff (FNAME, LNAME, PNUM) VALUES ("Luke", "Potter", 021674436);
INSERT INTO tbl_staff (FNAME, LNAME, PNUM) VALUES ("Justin", "Smith", 021465457);
INSERT INTO tbl_staff (FNAME, LNAME, PNUM) VALUES ("Gabrial", "Morningstar", 02135788);

use containerdb;

SELECT * FROM tbl_staff;