<?php
  //option, but easy: classname same as controller with leading underbar
  class _Pages {
    private $db;

    public function __construct() {
        $this->db = new Database;
    }
    public function getData(){
      $this->db->query('SELECT * FROM tbl_products');
      return $this->db->resultSet();

    }

    public function getTitles(){
      $this->db->query('SELECT TITLE FROM tbl_products');
      return $this->db->resultSet();

    }

    public function getDescript(){
      $this->db->query('SELECT DESCRIPT FROM tbl_products');
      return $this->db->resultSet();
    }

    public function getCost(){
      $this->db->query('SELECT PRICE FROM tbl_products');
      return $this->db->resultSet();
    }

    public function getImg(){
      $this->db->query('SELECT IMG FROM tbl_products');
      return $this->db->resultSet();
    }

    public function getCust(){
      $this->db->query('SELECT * FROM tbl_cust');
      return $this->db->resultSet();
    }

    public function getStaff(){
      $this->db->query('SELECT * FROM tbl_staff');
      return $this->db->resultSet();
    }


    // Any model functions go here
    // Can return DB data or static

    /////////////////////////////////////////////////
    //////////////// EXAMPLES ///////////////////////
    /////// YOU SHOULD DELETE THESE AFTER ///////////
    /////////////////////////////////////////////////

    //example: static data
    public function title() {
      return "Show All People";
    }

    //example: db data - select
    public function getAllPeople() {
      $this->db->query('SELECT * FROM tbl_products');
      return $this->db->resultSet();
    }

    //exaple: db data - insert
    public function addPerson($fn, $ln, $dob) {
      
      //Adding data to database
      $this->db->query('INSERT INTO  tbl_people (FNAME, LNAME, DOB) VALUES (:fn, :ln, :dob)');

      //Binding Variables
      $this->db->bind(':fn', $fn);
      $this->db->bind(':ln', $ln);
      $this->db->bind(':dob', $dob);

      //Return true or false, based on if query is successful or not
      if($this->db->execute()) {
          return true;
      } else {
          return false;
      }
    }
  }
?>