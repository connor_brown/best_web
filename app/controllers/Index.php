<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class  Index extends Controller {

      public function __construct() {
        $this->title = $this->model('_Pages'); //name your model
      }

      public function Home() {

        // variables for data or model functions go here
        
       $t = $this->title->getTitles();
       
       
        // add data or variables to the array using key-value pairs
        $data = [
          'title'=>$t
        ];

        // call your view 
        $this->view('pages/Home', $data);
      }
      public function Products(){


        // variables for data or model functions go here
        
        $t = $this->title->getTitles();
        $d = $this->title->getDescript();       
        $p = $this->title->getCost();
        $i = $this->title->getImg();
        $da = $this->title->getData();
        // add data or variables to the array using key-value pairs
        $data = [
          'data'=>$da,
          'title'=>$t,
          'descript'=>$d,
          'cost'=>$p,
          'img'=>$i
        ];

        $this->view('pages/Products', $data);
      }
      public function Contact(){
        $st = $this->title->getStaff();
        // add data or variables to the array using key-value pairs
        $data = [
          'staff'=>$st
        ];
        $this->view ('pages/Contact', $data);
      }
      public function Orders(){
        $c = $this->title->getCust();
        // add data or variables to the array using key-value pairs
        $data = [
          'cust'=>$c
        ];
        $this->view ('pages/Orders', $data);

      }
    }
